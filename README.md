It is a modification of [Instance insertion](https://github.com/NVlabs/Instance_Insertion)

Train:
```
python main.py
```

Generate data:
```
python generate_dataset.py
```
OR
```
python generate_dataset_wrt_jsons.py
```
