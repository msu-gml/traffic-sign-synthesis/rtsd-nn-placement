  # Copyright (c) 2018-2019, NVIDIA CORPORATION. All rights reserved.
  # 
  # Licensed under the CC BY-NC-SA 4.0 license 
  #     https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode
  #


import os.path
import numpy as np
from skimage import img_as_float
from skimage.io import imread
from skimage.transform import resize
import json

class Dataset(object):
    def __init__(self, opt,
                 json_path_train='/mydata/kursovaya4/BicycleGAN/data_rtsd/train_rtsd.json',
                 json_path_test='/mydata/kursovaya4/BicycleGAN/data_rtsd/test_rtsd.json',
                 rtsd_images_dir='/mydata/kursovaya4/BicycleGAN/data_rtsd/detection_data/',
                 is_test=False):
        self.opt = opt
        self.is_first = True
        self.rtsd_images_dir = rtsd_images_dir
        if not is_test:
            with open(json_path_train, 'r') as fp:
                json_data_full = json.load(fp)
        else:
            with open(json_path_test, 'r') as fp:
                json_data_full = json.load(fp)
        self.json_data = []
        self.filenames_to_idxs = []
        fnames = sorted(list(json_data_full.keys()))
        for fname in fnames:
            lst = json_data_full[fname]
            len_start = len(self.json_data)
            self.json_data.extend([(fname, item) for item in lst if (not item['ignore']) and item['x'] > 0 and item['y'] > 0 and item['h'] > 15 and item['w'] > 15])
            self.filenames_to_idxs.append((fname, [i for i in range(len_start, len(self.json_data))]))
        print("rtsdroad", len(self.json_data))
    
    def get_length(self):
        return len(self.json_data)
        
    def load_item(self, index, isFlip=False):
        if self.is_first:
            import random
            self.rnd = random.Random(index)
            self.is_first = False
        item = self.json_data[index % len(self.json_data)]
        fname = item[0]
        bbox_info = item[1]
        img = img_as_float(imread(self.rtsd_images_dir + fname))
        if isFlip:
            img = img[:, ::-1]
        img_big = resize(img, (self.opt.image_sizey_big, self.opt.image_sizex_big), mode='constant', anti_aliasing=True).astype(np.float32)
        img_small = resize(img, (self.opt.image_sizey_small, self.opt.image_sizex_small), mode='constant', anti_aliasing=True).astype(np.float32)
        obj_map_big = np.zeros((img_big.shape[0], img_big.shape[1], 1), dtype=np.uint8)
        y = bbox_info['y'] * img_big.shape[0] // img.shape[0]
        x = bbox_info['x'] * img_big.shape[1] // img.shape[1]
        h = bbox_info['h'] * img_big.shape[0] // img.shape[0]
        w = bbox_info['w'] * img_big.shape[1] // img.shape[1]
        #print(y, x, h, w, index)
        obj_map_big[y:y+h, x:x+w] = 255
        obj_map_small = np.zeros((img_small.shape[0], img_small.shape[1], 1), dtype=np.uint8)
        y = bbox_info['y'] * img_small.shape[0] // img.shape[0]
        x = bbox_info['x'] * img_small.shape[1] // img.shape[1]
        h = bbox_info['h'] * img_small.shape[0] // img.shape[0]
        w = bbox_info['w'] * img_small.shape[1] // img.shape[1]
        obj_map_small[y:y+h, x:x+w] = 255
        compact = np.ones((self.opt.compact_sizey, self.opt.compact_sizex))
        #return img_big.transpose([2, 0, 1]), img_small.transpose([2, 0, 1]), obj_map_big.transpose([2, 0, 1]), obj_map_small.transpose([2, 0, 1]), compact
        return img_big, img_small, obj_map_big, obj_map_small, compact
        



class DatasetPrev():
    def __init__(self, opt):
        self.opt = opt
        self.db_root = opt.db_root

    def load(self):
        self.target_class = self.opt.target_class

        if self.target_class == 'person':
            self.segID = '24'
            self.opt.target_channel = 20
            self.opt.min_size_percent = 0.2
            self.opt.min_area_ratio = 0.4
            self.opt.max_aspect_ratio = 4
            self.opt.max_width_ratio = 50
            self.opt.max_height_ratio = 10
        elif self.target_class == 'car':
            self.segID = '26'
            self.opt.target_channel = 22
            self.opt.min_size_percent = 0.5
            self.opt.min_area_ratio = 0.4
            self.opt.max_aspect_ratio = 3
            self.opt.max_width_ratio = 30
            self.opt.max_height_ratio = 10
        else:
            print('wrong object')

        self.seg_list, self.ins_list = self.cityscape_load_seg_and_ins(self.opt.db_root, quality='fine', mode='train')
        self.test_seg_list, self.test_ins_list = self.cityscape_load_seg_and_ins(self.opt.db_root, quality='fine', mode='val')

        self.opt.nClass = 29 + 1
        ignore_label = 0
        self.opt.ignore_label = ignore_label
        self.opt.chn_and_segID = {-1: 22, 0: ignore_label, 1: 1, 2: ignore_label,
                                  3: ignore_label, 4: ignore_label, 5: 2, 6: 3,
                                  7: 4, 8: 5, 9: 6, 10: 7, 11: 8, 12: 9, 13: 10,
                                  14: 11, 15: 12, 16: 13, 17: 14,
                                  18: 14, 19: 15, 20: 16, 21: 17, 22: 18, 23: 19, 24: 20, 25: 21, 26: 22,
                                  27: 23, 28: 24, 29: 25, 30: 26, 31: 27, 32: 28, 33: 29}

    def cityscape_load_seg_and_ins(self, root, quality, mode):
        assert (quality == 'fine' and mode in ['train', 'val']) or \
               (quality == 'coarse' and mode in ['train', 'train_extra', 'val'])

        if quality == 'coarse':
            seg_path = os.path.join(root, 'gtCoarse', 'gtCoarse', mode)
            seg_postfix = '_gtCoarse_labelIds.png'
            ins_postfix = '_gtCoarse_instanceIds.png'
        else:
            seg_path = os.path.join(root, 'gtFine', mode)
            seg_postfix = '_gtFine_labelIds.png'
            ins_postfix = '_gtFine_instanceIds.png'

        img_path = os.path.join(root, 'leftImg8bit', mode)
        seg_list = []
        ins_list = []
        categories = os.listdir(seg_path)
        for c in categories:
            c_items = [name.split('_leftImg8bit.png')[0] for name in os.listdir(os.path.join(img_path, c))]
            for it in c_items:
                seg_list.append(os.path.join(seg_path, c, it + seg_postfix))
                ins_list.append(os.path.join(seg_path, c, it + ins_postfix))
        return np.array(seg_list), np.array(ins_list)