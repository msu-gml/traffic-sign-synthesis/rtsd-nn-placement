  # Copyright (c) 2018-2019, NVIDIA CORPORATION. All rights reserved.
  # 
  # Licensed under the CC BY-NC-SA 4.0 license 
  #     https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode
  #

from __future__ import print_function
import random
import torch.utils.data
import time
import torch.backends.cudnn as cudnn

from options import *
from dataset import *
from utils import *
from model import InstanceAdder

######################################################### Options
opt = Options().parse()
print(opt)

print("Random Seed: ", opt.random_seed)
random.seed(opt.random_seed)
torch.manual_seed(opt.random_seed)
if opt.use_gpu:
    torch.cuda.manual_seed_all(opt.random_seed)

cudnn.benchmark = True

######################################################### Dataset
db_train = Dataset(opt)
db_test = Dataset(opt, is_test=True)
#db.load()

num_train = db_train.get_length()
print("Dataset done")

######################################################### Model
model = InstanceAdder(opt)
model.define_networks()
model.define_optimizers()
start_epoch=1
model.load(start_epoch-1)
print("Model done")

######################################################### Training
isFlip = False
nTest = 16
test_big, test_small, test_map_big, test_map_small = [], [], [], []
idxes = np.random.permutation(db_test.get_length())
for i in range(nTest):
    big, small, ob_big, ob_small, _ = db_train.load_item(idxes[i], isFlip)
    test_big.append(big)
    test_small.append(small)
    test_map_big.append(ob_big)
    test_map_small.append(ob_small)

test_z_appr = torch.FloatTensor(nTest, opt.z_dim_appr, 1, 1).normal_(0, 1)
test_z_spatial = torch.FloatTensor(nTest, opt.z_dim_spatial, 1, 1).normal_(0, 1)
test_z2_appr = torch.FloatTensor(nTest, opt.z_dim_appr, 1, 1).normal_(0, 1)
test_z2_spatial = torch.FloatTensor(nTest, opt.z_dim_spatial, 1, 1).normal_(0, 1)


num_img_rows = 4
num_img_cols = 12
im_h = opt.image_sizey_big
im_w = opt.image_sizex_big
image_save = Image.new('RGB',
                       (num_img_cols * im_w + (num_img_cols - 1) * 3,
                        num_img_rows * im_h + (num_img_rows - 1) * 3), 'green')

for t in range(num_img_cols):
    model.set_conditional_input([test_small[t]], [test_map_small[t]],
                                [test_big[t]], [test_map_big[t]])

    test_seg = colorize_mask(seg_to_single_channel(model.cond_seg_big.cpu().data[0].numpy(), 'chw'), 'cityscape')

    model.set_z(test_z_appr[t].unsqueeze(0), test_z_spatial[t].unsqueeze(0))
    model.forward(mode='test')
    bb1 = colorize_mask(seg_to_single_channel(model.bb_on_seg_big.cpu().data[0].numpy(), 'chw'), 'cityscape')

    model.set_z(test_z2_appr[t].unsqueeze(0), test_z_spatial[t].unsqueeze(0))
    model.forward(mode='test')
    bb2 = colorize_mask(seg_to_single_channel(model.bb_on_seg_big.cpu().data[0].numpy(), 'chw'), 'cityscape')

    model.set_z(test_z_appr[t].unsqueeze(0), test_z2_spatial[t].unsqueeze(0))
    model.forward(mode='test')
    bb3 = colorize_mask(seg_to_single_channel(model.bb_on_seg_big.cpu().data[0].numpy(), 'chw'), 'cityscape')

    image_save.paste(test_seg, ((im_w + 3) * t, (im_h + 3) * 0))
    image_save.paste(bb1, ((im_w + 3) * t, (im_h + 3) * 1))
    image_save.paste(bb2, ((im_w + 3) * t, (im_h + 3) * 2))
    image_save.paste(bb3, ((im_w + 3) * t, (im_h + 3) * 3))

save_name = "epoch_%02d_iter_%04d.png" % (epoch, b)
save_image_path = os.path.join(opt.sample_dir, save_name)

image_save.save(save_image_path)
aa = 2

