  # Copyright (c) 2018-2019, NVIDIA CORPORATION. All rights reserved.
  # 
  # Licensed under the CC BY-NC-SA 4.0 license 
  #     https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode
  #
from __future__ import print_function

import json
with open('/mydata/kursovaya4/BicycleGAN/data_rtsd/train_rtsd.json', 'r') as fp:
    json_data = json.load(fp)
cnts = []
rgb_shapes = {}
for k, v in json_data.items():
    import imagesize
    rgb_shape = imagesize.get('/mydata/kursovaya4/BicycleGAN/data_rtsd/detection_data/' + k)
    rgb_shapes[k] = rgb_shape
    cnts.append([len(v)])
from sklearn.neighbors import KernelDensity
kdf_cnts = KernelDensity()
kdf_cnts.fit(cnts)    
    
    
def my_iou(boxA, boxB):
    xA = max(boxA[0], boxB[0])
    yA = max(boxA[1], boxB[1])
    xB = min(boxA[2], boxB[2])
    yB = min(boxA[3], boxB[3])
    interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)
    boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
    boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)
    iou = interArea / float(boxAArea + boxBArea - interArea)
    return iou
with open('/mydata/kursovaya4/BicycleGAN/data_rtsd/train_rtsd.json', 'r') as fp:
    original_positions_json_data = json.load(fp)    
    
    
    
import random
import torch.utils.data
import time
import torch.backends.cudnn as cudnn

from options import *
from dataset import *
from utils import *
from model import InstanceAdder
import json

######################################################### Options
opt = Options().parse()
print(opt)

print("Random Seed: ", opt.random_seed)
random.seed(opt.random_seed + 6000)
torch.manual_seed(opt.random_seed + 6000)
if opt.use_gpu:
    torch.cuda.manual_seed_all(opt.random_seed + 6000)

cudnn.benchmark = True

######################################################### Dataset
db = Dataset(opt)
db.load()

num_train = len(db.seg_list)
print("Dataset done", num_train)

######################################################### Model
model = InstanceAdder(opt)
model.define_networks()
model.define_optimizers()
start_epoch=11
if start_epoch > 0:
    model.load(start_epoch-1)
print("Model done")

######################################################### Training
isFlip = False
nTest = 16
b_test_idx = np.random.permutation(num_train)

new_dataset = {}
for t in range(num_train):
    need_cnt = int(round(kdf_cnts.sample(1)[0][0]))
    cur_bboxes = []
    rects = []
    cnt_try_rect = 0
    img_fname = db.seg_list[b_test_idx[t]]["rgb_name"].split('/')[-1]
    orig_json_img_poss = original_positions_json_data[img_fname]
    print(orig_json_img_poss, img_fname)
    img_shape = rgb_shapes[img_fname]
    while len(cur_bboxes) < need_cnt:
        test_seg_list, test_ins_list = [db.seg_list[b_test_idx[t]]], [db.ins_list[b_test_idx[t]]]
        test_seg_small = prepare_input(test_seg_list, isFlip, opt, 'seg_small')
        test_seg_big = prepare_input(test_seg_list, isFlip, opt, 'seg_big')
        test_ins = prepare_input(test_ins_list, isFlip, opt, 'insMap')
        test_ins_box = [test_ins[i].resize([opt.image_sizex_small, opt.image_sizey_small]) for i in range(1)]
        test_ins_box = [np.expand_dims(np.array(test_ins_box[i]), 2) for i in range(1)]
        test_ins_shape = [test_ins[i].resize([opt.image_sizex_big, opt.image_sizey_big]) for i in range(1)]
        test_ins_shape = [np.expand_dims(np.array(test_ins_shape[i]), 2) for i in range(1)]
        test_z_appr = [torch.FloatTensor(1, opt.z_dim_appr, 1, 1).normal_(0, 1) for _ in range(1)]
        test_z_spatial = [torch.FloatTensor(1, opt.z_dim_spatial, 1, 1).normal_(0, 1) for _ in range(1)]

        model.set_conditional_input([test_seg_small[0]], [test_ins_box[0]],
                                    [test_seg_big[0]], [test_ins_shape[0]])

        model.set_z(test_z_appr[0][0].unsqueeze(0), test_z_spatial[0][0].unsqueeze(0))
        model.forward(mode='test')
        bb3_add = seg_to_heatmap(model.bb_padded_big.cpu().data[0].numpy(), 'chw')
        x = np.argwhere(bb3_add)
        if len(x) > 0:
            y, x, h, w = x[0][0], x[0][1], x[-1][0] - x[0][0], x[-1][1] - x[0][1]
            y = y * img_shape[1] // 512
            h = h * img_shape[1] // 512
            x = x * img_shape[0] // 1024
            w = w * img_shape[0] // 1024
            h = max(h, 16)
            w = max(w, 16)
            x = min(x, img_shape[0] - w)
            y = min(y, img_shape[1] - h)
            bbox = {
                "w" : int(w),
                "h" : int(h),
                "x" : int(x),
                "y" : int(y),
                "ignore" : False,
                "sign_class": "0",
            }
            rect = [x, y, x + w, y + h]
            isok = True
            if (w * h >= 128 * 128):
                isok = False;
            for rjs in orig_json_img_poss:
                rA = [rjs['x'], rjs['y'], rjs['x'] + rjs['w'], rjs['y'] + rjs['h']]
                if isok and my_iou(rect, rA) != 0:
                    isok = False
            for rjs in cur_bboxes:
                rA = [rjs['x'], rjs['y'], rjs['x'] + rjs['w'], rjs['y'] + rjs['h']]
                if isok and my_iou(rect, rA) != 0:
                    isok = False
            if isok or cnt_try_rect == 50:
                cnt_try_rect = 0
                cur_bboxes.append(bbox)
                print(t, len(cur_bboxes), bbox)
            else:
                cnt_try_rect += 1
    if len(cur_bboxes) > 0:
        new_dataset[img_fname] = cur_bboxes
    with open("deeplabinsins_generated_bboxes_wrt_jsons2.json", "w") as fp:
        json.dump(new_dataset, fp)
